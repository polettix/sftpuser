#!/bin/sh
# vim: ft=sh sw=3 ts=3 sts=3 et ai :

### DEFAULTS ####
default_environment() {
   : ${SFTPUSER_BASEDIR="/var/sftp"}
   : ${SFTPUSER_FAKE="0"}
   : ${SFTPUSER_SUDO="sudo"}
   : ${SFTPUSER_RGROUP="sftpreader"}
   : ${SFTPUSER_WGROUP="sftpwriter"}

   DEBUG "$(cat <<END
configuration:
       base dir: '$SFTPUSER_BASEDIR'
           fake: '$SFTPUSER_FAKE'
           sudo: '$SFTPUSER_SUDO'
        verbose: '$SFTPUSER_VERBOSE'
   reader group: '$SFTPUSER_RGROUP'
   writer group: '$SFTPUSER_WGROUP'
END
   )"
}

############################################################################
#
# Commands
#

command_commands() { #<C>
#<H> usage: commands
#<H> print a list of available commands.
   {
   	printf '%s available (sub-)commands:\n' "$0"
      sed -ne '/#<C> *$/s/command_\([a-zA-Z0-9_]*\).*/- \1/p' "$0"
   } >&2
}

command_create() { #<C>
#<H> usage: create <username>
#<H> create a new user
   local username="$1"
   ! _check_id "$username" || LOGDIE "user '$username' already exists"
   local userdir="$(_userdir_frozen "$1")"
   _useradd -M -U -G "$SFTPUSER_RGROUP" -s /sbin/nologin "$username"
   _sudo_or_fake passwd "$username"
   _sudo mkdir -p "$userdir"
   command_thaw "$username"
}

command_freeze() { #<C>
#<H> usage: freeze <username>
#<H> freeze the sftp directory for username
   local username="$1"
   local frozen="$(_userdir_frozen "$username")"
   local thawed="$(_userdir_thawed "$username")"
   _assert_managed "$username"
   if _check_frozen "$username" ; then
      _sudo mv "$frozen" "$thawed" || LOGDIE "bail out"
   fi
   _sudo_or_fake chown -R "$username" "$thawed" || LOGDIE "bail out"
   _sudo_or_fake chown root:root "$thawed" || LOGDIE "bail out"
   _sudo chmod -R og+r,a-w "$thawed" || LOGDIE "bail out"
   _sudo mv "$thawed" "$frozen" || LOGDIE "cannot freeze $username"
   return 0
}

command_help() { #<C>
#<H> usage: help
#<H> print help for all available commands
   {
      printf '\nUsage: %s <command> [<arg> [...]]\n\n' "$0"
      printf 'Available (sub-)commands:\n'
      sed -ne '
         /#<C> *$/s/command_\([a-zA-Z0-9_]*\).*/\n- \1/p
         s/^#<H> /    /p
         s/^#<H>//p
      ' "$0"
   } >&2
}

command_init() { #<C>
#<H> usage: init
#<H> initialize the system (create groups and directories)
   _ensure_group "$SFTPUSER_RGROUP"
   _ensure_group "$SFTPUSER_WGROUP"
   _sudo mkdir -p "$SFTPUSER_BASEDIR/frozen" "$SFTPUSER_BASEDIR/thawed"
   _sudo_or_fake chown root:root "$SFTPUSER_BASEDIR" \
      "$SFTPUSER_BASEDIR/frozen" "$SFTPUSER_BASEDIR/thawed"
   _sudo chmod og-w "$SFTPUSER_BASEDIR" \
      "$SFTPUSER_BASEDIR/frozen" "$SFTPUSER_BASEDIR/thawed"
}

command_list() { #<C>
#<H> usage: list
#<H> list all currently ongoing users

   printf 'FROZEN\n'
   _sudo ls -1 "$SFTPUSER_BASEDIR/frozen" | sed 's/^/  - /'
   printf 'THAWED\n'
   _sudo ls -1 "$SFTPUSER_BASEDIR/thawed" | sed 's/^/  - /'
}

command_remove() { #<C>
#<H> usage: remove <username>
#<H> remove a username
   local username="$1"
   _assert_managed "$username"
   _userdel  "$username"
   _sudo_or_fake groupdel "$username" >/dev/null 2>&1 || true
   _rmdir "$(_userdir_frozen "$username")"
   _rmdir "$(_userdir_thawed "$username")"
}

command_status() { #<C>
#<H> usage: status <username>
#<H> show status of specific username
   local username="$1"
   if ! _check_id "$username" ; then
      DEBUG "user '$username' does not exist"
      printf %s\\n ENOTFOUND
      return 1
   fi
   if [ -d "$(_userdir_frozen "$username")" ]; then
      DEBUG "user '$username' can download files (frozen directory)"
      printf %s\\n FROZEN
      return 0
   fi
   if [ -d "$(_userdir_thawed "$username")" ]; then
      DEBUG "user '$username' cannot yet download files (thawed directory)"
      printf %s\\n THAWED
      return 0
   fi
   DEBUG "user '$username' has no directory for sftp"
   printf %s\\n ENOSFTP
   return 2
}

command_thaw() { #<C>
#<H> usage: thaw <username>
#<H> thaw (un-freeze) the sftp directory for username
   local username="$1"
   local frozen="$(_userdir_frozen "$username")"
   local thawed="$(_userdir_thawed "$username")"
   _assert_managed "$username"
   if _check_frozen "$username" ; then
      _sudo mv "$frozen" "$thawed" || LOGDIE "cannot thaw $username"
   fi
   _sudo chmod -R ug+rx  "$thawed" || LOGDIE "bail out"
   _sudo chmod    ug+rwx "$thawed" || LOGDIE "bail out"
   _sudo_or_fake chgrp -R "$SFTPUSER_WGROUP" "$thawed" || LOGDIE "bail out"
   return 0
}


############################################################################
#
# Support functions
#

_assert_frozen() { _check_frozen "$1" || LOGDIE "user $1: not frozen"; }

_assert_managed() { _check_managed "$1" || LOGDIE "user $1: unmanaged"; }

_assert_thawed() { _check_thawed "$1" || LOGDIE "user $1: not thawed"; }

_check_id() {
   if _is_fake ; then
      local users_file="$(dirname "$SFTPUSER_BASEDIR")/users"
      grep >/dev/null 2>&1 "$1" "$users_file"
   else
      id "$@" >/dev/null 2>&1
   fi
}

_check_frozen() {
   local username="$1"
   _check_id "$username" || return 1
   [ -d "$(_userdir_frozen "$username")" ] || return 2
   return 0
}

_check_managed() { _check_frozen "$1" || _check_thawed "$1" || return 1; }

_check_thawed() {
   local username="$1"
   _check_id "$username" || return 1
   [ -d "$(_userdir_thawed "$username")" ] || return 2
   return 0
}

_ensure_group() {
   local groupname="$1"
   cat /etc/group | grep "^$groupname:" >/dev/null 2>&1 \
      || _sudo_or_fake groupadd "$groupname"
}

_is_fake() { is_var_true SFTPUSER_FAKE ; }

_rmdir() {
   local dir="$1"
   [ -d "$dir" ] || return 0
   if _is_fake ; then
      chmod u+w "$dir"
   fi
   _sudo rm -rf "$dir"
}

_sudo() {
   if _is_fake ; then
      "$@"
   else
      $SFTPUSER_SUDO "$@"
   fi
}

_sudo_or_fake() {
   if _is_fake ; then
      WARN "would <$*> now..."
   else
      _sudo "$@"
   fi
}

_useradd() {
   if _is_fake ; then
      local users_file="$(dirname "$SFTPUSER_BASEDIR")/users"
      printf %s\\n "$*" >>"$users_file"
   else
      _sudo useradd "$@"
   fi
}

_userdel() {
   if _is_fake ; then
      local users_file="$(dirname "$SFTPUSER_BASEDIR")/users"
      sed -i -e "/$1/d" "$users_file"
   else
      _sudo userdel "$@"
   fi
}

_userdir_frozen() { printf %s "$SFTPUSER_BASEDIR/frozen/$1"; }

_userdir_thawed() { printf %s "$SFTPUSER_BASEDIR/thawed/$1"; }


############################################################################
# Various utilities to protect strings
# See https://github.polettix.it/ETOOBUSY/2020/03/22/shell-quoting-for-exec/
# See https://github.polettix.it/ETOOBUSY/2020/03/23/shell-dynamic-args/

array_freeze() { #<command>
#<help> usage: array_freeze [<arg> [<arg> [...]]]
#<help> freeze an argument array into a single string, printed on standard
#<help> output. When collected in $string, the argument array can be
#<help> restored with:
#<help>      exec "set -- $string"
   local i
   for i do
      printf '%s\n' "$i" | sed "s/'/'\\\\''/g;1s/^/'/;\$s/\$/' \\\\/"
   done
   printf ' '
}

quote () { #<command>
#<help> usage: quote <string-to-quote-as-a-single-argument>
#<help> quote a string to be used in exec and its siblings (e.g. remote ssh)
   printf %s\\n "$1" | sed "s/'/'\\\\''/g;1s/^/'/;\$s/\$/'/"
}

############################################################################
# Logging functions
# See https://github.polettix.it/ETOOBUSY/2020/03/24/shell-logging-helpers/
_LOG() {
   : ${LOGLEVEL:='INFO'}
   LEVELS='
TRACE  TRACE DEBUG INFO WARN ERROR FATAL
DEBUG        DEBUG INFO WARN ERROR FATAL
INFO               INFO WARN ERROR FATAL
WARN                    WARN ERROR FATAL
ERROR                        ERROR FATAL
FATAL                              FATAL
   '
   local timestamp="$(date '+%Y-%m-%dT%H%M%S%z')"
   if printf '%s' "$LEVELS" \
         | grep "^$LOGLEVEL .* $1" >/dev/null 2>&1 ; then
      printf >&2 '[%s] [%5s] %s\n' "$timestamp" "$@"
   fi
}

set_LOGLEVEL() { #<command>
#<help> usage: set_LOGLEVEL <level>
#<help> set the LOGLEVEL variable to `level`, which acts as a threshold
#<help> for printing messages. Choose one of the available levels:
#<help> TRACE DEBUG INFO WARN ERROR FATAL
   LEVELS='
xTRACE
xDEBUG
xINFO
xWARN
xERROR
xFATAL
'
   if printf '%s' "$LEVELS" | grep "^x$1$" >/dev/null 2>&1 ; then
      LOGLEVEL="$1"
   else
      printf 'Invalid log level <%s>, using INFO instead\n' "$1"
      LOGLEVEL='INFO'
   fi
}

TRACE()  { _LOG TRACE "$*"; }    #<command>
#<help> usage: TRACE message
#<help> output a log message at TRACE level, if enabled

DEBUG()  { _LOG DEBUG "$*"; }    #<command>
#<help> usage: DEBG message
#<help> output a log message at DEBUG level, if enabled

INFO()   { _LOG INFO  "$*"; }    #<command>
#<help> usage: INFO message
#<help> output a log message at INFO level, if enabled

WARN()   { _LOG WARN  "$*"; }    #<command>
#<help> usage: WARN message
#<help> output a log message at WARN level, if enabled

ERROR()  { _LOG ERROR "$*"; }    #<command>
#<help> usage: ERROR message
#<help> output a log message at ERROR level, if enabled

FATAL()  { _LOG FATAL "$*"; }    #<command>
#<help> usage: FATAL message
#<help> output a log message at FATAL level, if enabled

LOGDIE() { FATAL "$*"; exit 1; } #<command>
#<help> usage: LOGDIE message
#<help> output a log message at FATAL level and exit with code 1


############################################################################
# Test functions.
# See: https://github.polettix.it/ETOOBUSY/2020/03/25/shell-variable-is_defined/
# See: https://github.polettix.it/ETOOBUSY/2020/03/26/shell-variable-is_true/
# See: https://github.polettix.it/ETOOBUSY/2020/03/27/shell-variable-is_lengthy/

is_var_defined () { eval "[ -n \"\${$1+ok}\" ]" ; } #<command>
#<help> usage: is_var_defined <variable-name>
#<help> test whether `variable-name` is defined (i.e. set) or not

is_var_true() { #<command>
#<help> usage: is_var_true <variable-name>
#<help> test whether `variable-name` holds a true value. An undefined variable
#<help> is false. Empty and 0 values are false. Everything else is true.
   local value
   eval 'value="${'"$1"':-"0"}"'
   [ "$value" != '0' ]
}

is_value_true() { #<command>
#<help> usage: is_value_true [<value>]
#<help> test whether `value` is true. An empty input list is false. If $1
#<help> is set, empty and 0 values are false. Everything else is true.
   [ $# -gt 0 ] || return 1    # empty input list -> false
   [ "${1:-"0"}" != '0' ]
}

is_var_lengthy() { #<command>
#<help> usage: is_var_lengthy <variable-name>
#<help> test whether <variable-name> is set and holds a non-empty value.
   local value
   eval 'value="${'"$1"':-""}"'
   [ -n "$value" ]
}
is_value_lengthy() { [ $# -gt 0 ] && [ -n "$1" ] ; } #<command>
#<help> usage: is_value_lengthy [<value>]
#<help> test whether the argument list is not empty and the first value
#<help> is not empty as well.


############################################################################
# Everything as a sub-command.
# See https://github.polettix.it/ETOOBUSY/2020/03/19/a-shell-approach/
# Hint: keep this at the bottom and change the string with something
# new/random for each new script.

#<help>
if grep -- 'sftpuser-db049fa26dd59' "$0" >/dev/null 2>&1
then
   [ $# -gt 0 ] || set -- help
   command="$1"
   shift 1
   default_environment
   "command_$command" "$@"
fi
