`sftpuser` is a small utility to manage a SFTP-only server, based on
[OpenSSH][].

# SYNOPSIS

Initialize everything:

```
$ sftpuser init
```

One or more account should be added to the *writers* group, by default
`sftpwriter`:

```
$ usermod -a -G sftpwriter $writer_username
```

Create an account for reading, named `transfer-01`:

```
$ sftpuser create transfer-01
```

Now the writer account can put files inside
`/var/sftp/thawed/transfer-01` (by default). When ready, the directory
can be frozen and exposed externally:

```
$ sftpuser freeze transfer-01
```

At this point the directory is available in
`/var/sftp/frozen/transfer-01` and accessible from user `transfer-01`
(read-only).

When the user ends downloading files it's possible to clean things up:

```
$ sftpuser remove transfer-01
```

# OTHER FILES

The repository contains other utility files.

## SSHD Configuration

File `sshd_config` can be used for setting up the configuration of the
[OpenSSH][] server. Ensure that the two groups that are enabled are the
same used with `sftpuser` (which is the case, by default).

## IPTables Configuration

The repository also contains files to configure `iptables` to restrict
only SSH access and nothing more.

# AUTHOR

Flavio Poletti <flavio@polettix.it>

# COPYRIGHT AND LICENSE

Copyright 2021 by Flavio Poletti <flavio@polettix.it>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

[OpenSSH]: https://www.openssh.com/
